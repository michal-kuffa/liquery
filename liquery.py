#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import urllib
import scrapy
import scrapy_splash
from w3lib.html import remove_tags, replace_entities


class Person(scrapy.Item):
    '''
    Representation of a person from LinkedIN search results.
    '''
    first_name = scrapy.Field()
    last_name = scrapy.Field()

    #: Position the person holds at the company
    position = scrapy.Field()
    company = scrapy.Field()

    #: City the person is based at (not company)
    city = scrapy.Field()

    #: Country the person is based at (not company)
    country = scrapy.Field()

    @classmethod
    def from_search_result(cls, person):
        '''
        Instantiate :class:`Person` instance from given `person` search result.

        :param person: Partial search result representing one person.
        :type person: :class:`scrapy.selector.unified.Selector`

        :returns: Instance of :class:`Person`
        '''
        base_div = person.css('div.bd')
        first_name, last_name = cls._parse_name(base_div)
        position, company = cls._parse_position_and_company(base_div)
        city, country = cls._parse_city_and_country(base_div)
        return cls(
            first_name=first_name,
            last_name=last_name,
            position=position,
            company=company,
            city=city,
            country=country,
        )

    @staticmethod
    def _parse_city_and_country(base_div, delimiter=', '):
        city_and_country = base_div.css('dl.demographic dd bdi').xpath('text()').extract()[0]
        city, country = None, city_and_country
        if delimiter in city_and_country:
            city, country = city_and_country.rsplit(delimiter, 1)
        return city, country

    @staticmethod
    def _parse_name(base_div):
        full_name = base_div.css('h3 a')[0].xpath('text()').extract()[0]
        return full_name.rsplit(' ', 1)

    @staticmethod
    def _parse_position_and_company(base_div, delimiters=(' at ', ' bei ')):
        position_and_company = replace_entities(
            remove_tags(base_div.css('.description')[0].extract()))
        position, company = None, None
        for delimiter in delimiters:
            if delimiter in position_and_company:
                return position_and_company.rsplit(delimiter, 1)
        return position, company


class LIQueryPeopleSpider(scrapy.Spider):
    name = 'people'
    custom_settings = {
        'FEED_FORMAT': 'csv',
        # Set explicit field order
        'FEED_EXPORT_FIELDS': [
            'first_name', 'last_name',
            'position', 'company',
            'city', 'country'
        ],
        'USER_AGENT': (
            'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; '
            'WOW64; Trident/5.0; SLCC2; Media Center PC 6.0; '
            'InfoPath.3; MS-RTC LM 8; Zune 4.7)')
    }

    def __init__(self, query='fleet manager', cookiefile=None, **kwargs):
        # Ensure default value for search query and cookiefile which may be
        # passed as a cli argument `-a query="my search query"`
        # Otherwise we will have to use getattr/hasattr to check it's existence
        kwargs['query'] = query
        kwargs['cookiefile'] = cookiefile
        super(LIQueryPeopleSpider, self).__init__(**kwargs)

    def start_requests(self):
        self.logger.info('Query string: %s', self.query)
        cookies = {}
        if self.cookiefile:
            cookies = json.load(open(self.cookiefile))
        self.logger.info('Cookies: %s', cookies)
        # yield scrapy.Request('file:///tmp/fleet_manager.html')
        yield scrapy.Request(
            u'{base_url}?{query}'.format(
                base_url='https://www.linkedin.com/vsearch/p',
                query=urllib.urlencode({
                    'orig': 'TRNV',
                    'keywords': self.query,
                })
            ),
            headers= {
                'User-Agent': (
                    'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; '
                    'WOW64; Trident/5.0; SLCC2; Media Center PC 6.0; '
                    'InfoPath.3; MS-RTC LM 8; Zune 4.7)')
            },
            cookies=cookies,
            meta={
                'splash': {
                    'args': {
                        'html': 1,
                        'png': 1,
                    },
                    'endpoint': 'render.json',
                    'slot_policy': scrapy_splash.SlotPolicy.PER_DOMAIN,
                    'splash_url': 'http://localhost:8050',
                }
            }
        )

    def parse(self, response):
        self.logger.info('Got response from url \'%s\'', response.url)
        people_result = response.css('.result.people')
        self.logger.info('Found %s people in search results', len(people_result))
        for person in people_result:
            yield Person.from_search_result(person)
