#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import json
import argparse
import requests
from bs4 import BeautifulSoup


def main():
    parser = argparse.ArgumentParser(description='Get LinkedIN login cookies.')
    parser.add_argument(
        'email', metavar='EMAIL', type=str,
        help='Email (session_key)')
    parser.add_argument(
        'passwd', metavar='PASS', type=str,
        help='Password (session_password)')
    args = parser.parse_args()
    login_url = 'https://www.linkedin.com/uas/login'
    session = requests.Session()
    session.headers = {
        'User-Agent': (
            'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; '
            'WOW64; Trident/5.0; SLCC2; Media Center PC 6.0; '
            'InfoPath.3; MS-RTC LM 8; Zune 4.7)')
    }
    response = session.get(login_url)
    soup = BeautifulSoup(response.content, 'lxml')
    csrf = soup.find(id="loginCsrfParam-login")['value']
    login_form_action = 'https://www.linkedin.com/uas/login-submit'
    response = session.post(
        login_form_action,
        data={
            'session_key': args.email,
            'session_password': args.passwd,
            'loginCsrfParam': csrf,
        }
    )
    cookies = requests.utils.dict_from_cookiejar(session.cookies)
    sys.stdout.write(json.dumps(cookies))
    sys.stdout.write('\n')


if __name__ == "__main__":
    main()
