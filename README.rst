
=========================================
LinkedIN search results (contact) scraper
=========================================

Finds the first contacts returned by the search on the first page and stores
them in a CSV file. For each contact the following information are included (if
present):

* First name

* Last name

* Position

* Company

* City the person is based at

* Country the person is based at


*Simplifying assumptions:*

* Anything before the last space in a person's name is first name.

* Position and company are always separated with the token ' at ' or ' bei '.
  Everything before the token is the position, everything after the token is
  the company.

* Valid login cookies have to be passed in from file as cli argument.


Installation
============


.. code-block:: bash

        pip install -r requirements.txt


Usage
=====

First you need valid cookies. ``liquery.py`` expects it to be json serialized
session cookies.

You can use ``licookie.py`` to generate such json file for you. ``licookie.py`` has additional requirements:

.. code-block:: bash

        pip install beautifulsoup4 requests

.. code-block:: bash

        python licookie.py email@example.com password > cookies.json


Start `Splash <https://github.com/scrapinghub/splash>`_ e.g. with docker:


.. code-block:: bash

        docker run -p 8050:8050 scrapinghub/splash


Last environment preparation (not needed in case of projects):

.. code-block:: bash

        export SCRAPY_SETTINGS_MODULE=splash_settings
        # or you can put the splash_settings.py to some already known to python location
        export PYTHONPATH=$(pwd)



Finaly scrape some contacts:


.. code-block:: bash

        scrapy runspider liquery.py \
                -o output.csv \
                -a query="search query" \
                -a cookiefile="/path/to/cookiefile.json"
